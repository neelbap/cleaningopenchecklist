# Cleaningopenchecklist

Aplicación para organizar las tareas del hogar

## Descripción

¿Estás esperando a que los platos se laven solos? Perfecto! Sigue esperando, ya que esta aplicación solo nos permitirá abordar las tareas del hogar de una forma fácil y sencilla.
Aunque estas tareas no son motivo suficiente para romper una relación y generar nuevas enemistades, comprendemos que hasta lo más absurdo puede ayudar.
Esperamos salvar muchas relaciones, ayudando con la asignación de tareas y responsabilidades. Para nosotros es importante favorecer el equilibrio y la armonía en el hogar.

En cada hogar, crearemos un usuario para cada integrante. Esto nos permitira designar los cronogamas semanales.

La aplicacion ya trae por defecto una lista de tareas. Podemos comenzar con esta lista, modificarla o crear una nueva. Esta parte es importante ya que si quedan tareas en el hogar no listadas, significa que nadie las hará. 

El día domingo de cada semana se repartiran las tareas entre todos los usuarios.

En cada

#  ESPECIFICACIÓN DE REQUERIMIENTOS BAJO NORMA IEEE830

## 1. Introducción

Este documento describe la Especificación de Requisitos de Software (ERS) para el Desarrollo de la aplicación móvil "cleaningopenchecklist". Esta especificación se ha estructurado en base a las directrices establecidas por el estándar IEEE práctica recomendada para Especificaciones de Requisitos Software ANSI/IEEE 830, 1998.

### 1.1 Propósito

El propósito es desarrollar una aplicación móvil que ayudará a los integrantes de un hogar a planificar y realizar las tareas de limpieza de la casa. En principio, funcionará solamente de manera offline (sin conexión a internet). Para esto, un dispositivo funcionará como administrador y guardara los progresos de manera local.

### 1.2 Ámbito del Sistema

* La aplicación móvil a desarrollar se llamará cleaningopenchecklist, contará con una interfaz gráfica de fácil uso que permitirá interactuar y consultar todas las tareas que debemos realizar en el día y cuales nos tocarán en la semana. 

* La interfaz gráfica deberá contar un calendario semanal para permitir al usuario tener conocimiento de las tareas que se han compleatado y cuales debe realizar. 

* cleaningopenchecklist, deberá interactuar con la base de datos almacenada en el dispositivo de uno de los usuarios. Este dispositivo es el que va a registrar y administrar todas las actividades o tareas hogareñas.

### 1.3 Definiciones, Acronimos y Abreviaturas

* Usuario: Persona que puede ingresar a la aplicación móvil a través de un proceso de autenticación utilizando nombre de usuario y contraseña.

* Logueo: Es el proceso mediante el cual se controla el acceso individual a un sistema
informático. 
 
* Requerimientos: Descripción de las necesidades a las que debe responder el producto a desarrollar.

* App: Es una abreviatura en ingles de la palabra aplication, es decir; es un programa
que posee características especiales destinado específicamente para dispositivos
móviles, también conocido como aplicación mobil.

Dispositivo móvil: Son aparatos pequeños capaces de procesar información y
conectarse a una red 

### 1.4 Referencias

* IEEE Recommended Practice for Software Requirements Specifications, IEEE Std 830-1998, vol., no., pp. 1,40, Oct. 20 1998. doi: 10.1109/IEEESTD.1998.88286 

### 1.5 Vision General del Documento

## 2. Descripción General

La aplicación móvil "cleaningopenchecklist" presentara una página inicial, la misma que permite escoger que tipo de usuario desea loguearse (administrador o usuario), para así poder presentar los diferentes tipos de servicios a los que tienen acceso cada usuario.
La cuenta tipo "usuario" solo permitirá visualizar la información seleccionada sin poder modificarla o eliminarla. Es importante señalarlo, ya que este tipo de cuenta no podra eliminar ni agregar tareas. 
La cuenta tipo "administrador" se encargará de asignar tareas a los demás usuarios (forma automatica), agregar nuevas tareas y modificar la lista de tareas. Será responsable de agregar nuevos usuarios al grupo
cleaningopenchecklist, es compatible con cualquier dispositivo móvil que contenga android superior a la version 6.

### 2.1 Perspectiva del Producto


### 2.2 Funciones del Producto 

La aplicación cleaningopenchecklist, permitirá realizar las siguientes funciones:

* Solicitar al usuario que inicie sesión. Aprovechandose de la base local almacenada en uno de los dispositivos.

* Mostrar de manera simple y sencilla cada tarea que el usuario debe realizar. 

* 

### 2.3 Caraterísticas de los Usuarios

Los usuarios principales serán integrantes de una vivienda, los mismos deberán contar con un dispositivo móvil. El nivel de experiencia necesario para la aplicación es equiparable al uso de cualquier gestor de tareas. Incluso, al tratarse de una lista de tareas que semanalmente se repiten, la experiencia necesaria es la básica: inicio de sesión y navegación intuitiva dentro de la app y confirmar cuando se ha finalizado una tarea.

### 2.4 Restricciones

* El sistema deberá tener un diseño e implementación sencilla, independiente de la plataforma o del lenguaje de programación usado.

* Todo el material que se realice para el usuario y la aplicación debe de estar en lenguaje español.

* El dispositivo sobre el que corra la aplicación debe ser Android con versión 4.4 o posterior.

* Los datos de los usuarios (nombres de usuarios y tareas) deben estar protegidos para que no sean accedidos por otros, ya que se trata de información personal y por lo tanto privada.

* 

### 2.5 Suposiciones y Dependencias

#### Suposición

Se asume que los requisitos descritos en este documento son estables una vez que sea aprobada su versión final.

Los equipos en los que se vaya a ejecutar la aplicación deben cumplir los requisitos antes indicados para garantizar una ejecución correcta de la misma.

Cualquier petición de cambios en la especificación debe ser aprobada por todas las partes y gestionada por el grupo de Gestión de la Configuración.

El usuario posee una cuenta en la tienda de aplicaciones de la plataforma correspondiente para poder descargarla.

Se cumplirán en general las restricciones indicadas anteriormente.


#### Dependencias

El usuario de la aplicación será capaz de utilizar un dispositivo.

La aplicación funcionará con la base de datos local, almacenada en uno de los dispositivos. La cual se encargará de gestionar las tareas y a quien debe asignarlas

El uso de la misma dependerá de la disponibilidad del dispositivo móvil y el tipo de sistema operativo que tenga.

### 2.6 Requisitos Futuros

* Se proyecta agregar una funcionalidad de manera Online, para evitar recargar y/o depender de un dispositivo movil administrador.

* Agregar minijuegos con un sistema de puntajes. Esto permitirá a los usuarios divertirse utilizando las funciones basica de la aplicacion.

* Intercambio de tareas entre usuarios. 

* Lanzamientos periódicos de actualización de errores, aproximadamente cada tres meses

* Aplicación compatible para Apple iOS.

* Agregar nuevos idiomas

### 3 Requisitos Específicos
### 3.1 Interfaces Externas
### 3.2 Funciones
### 3.3 Requisitos de Rendimiento
### 3.4 Restricciones de Diseño
### 3.5 Atributos del Sistema
### 3.6 Otros Requisitos

## 4 Apendices
